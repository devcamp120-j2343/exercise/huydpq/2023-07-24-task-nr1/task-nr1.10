const { default: mongoose } = require("mongoose");
const userModel = require("../model/userModel")
const diceHistoryModel = require("../model/diceHistoryModel");

// create
const createDiceHistory = async (req, res) => {
    let { user, dice } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: `user is invalid`
        })
    }
    var newDice = {
        _id: new mongoose.Types.ObjectId(),
        user,
        dice: Math.floor(Math.random() * 6) + 1
    }

    try {
        const createdDice = await diceHistoryModel.create(newDice);

        return res.status(201).json({
            status: "Create review successfully",
            createdDice
        })

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}
// get all Dice History 

const getAllDiceHistory = async (req, res) => {
    let id = req.query.userId
    try {
        if (id !== undefined) {
            const getAllByUser = await diceHistoryModel.find({user: id})
            return res.status(200).json({
                message: "Get DiceHistory Sucessfully",
                data: getAllByUser
            })
        } else {
            const getAll = await diceHistoryModel.find()

            return res.status(200).json({
                message: "Get All DiceHistory Sucessfully",
                data: getAll
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Internal server error",
            err: error.message
        })
    }

}
// get by id
const getDiceHistoryById = async (req, res) => {
    let id = req.params.diceId;

    if (id === undefined || !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: `id is invalid`
        })
    }

    diceHistoryModel.findById(id)
        .then(data => {
            return res.status(200).json({
                message: "Get All DiceHistory Sucessfully",
                data
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: "Internal server error",
                err: error.message
            })
        })
}
const updateDiceHistory = (req, res) => {
    let id = req.params.diceId;
    let { user, dice } = req.body;

    if (id === undefined || !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: `id is invalid`
        })
    }

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: `user is invalid`
        })
    }
    var newDice = {
        user,
        dice: Math.floor(Math.random() * 6) + 1
    }

    diceHistoryModel.findByIdAndUpdate(id, newDice)
        .then(data => {
            return res.status(200).json({
                message: "Get DiceHistory By ID Sucessfully",
                data
            })
        })
        .catch(err => {
            return res.status(500).json({
                message: "Internal server error",
                err: err.message
            })
        })
}
const deleteDiceHistory = async (req, res) => {
    let id = req.params.diceId;

    if (id === undefined || !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            message: `id is invalid`
        })
    }
    try {
        const deleteDice = diceHistoryModel.findByIdAndDelete(id)
        if(deleteDice){
            return res.status(200).json({
                message: "Delete DiceHistory By ID Sucessfully",
                deleteDice
            })
        } else {
            return res.status(201).json({
                message: "Not found any dicehistory",
                deleteDice
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Internal server error",
            err: err.message
        })
    }
}

module.exports = { createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistory, deleteDiceHistory }