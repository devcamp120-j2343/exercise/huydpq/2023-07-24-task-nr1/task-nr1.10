const { default: mongoose } = require("mongoose");
const userModel = require("../model/userModel");

// Create User
const createUser = (req, res) => {
    let body = req.body;

    if(!body.username){
        return res.status(400).json({
            message: `username is invalid`
        })
    }
    if(!body.firstname){
        return res.status(400).json({
            message: `username is invalid`
        })
    }
    if(!body.lastname){
        return res.status(400).json({
            message: `username is invalid`
        })
    }
    let newUser = {
        _id: new mongoose.Types.ObjectId(),
        username: body.username,
        firstname:body.firstname,
        lastname: body.lastname
    }
    userModel.create(newUser)
    .then( data => res.status(200).json({
        message: `Create User Sucessfully`,
        data
    }))
    .catch(error => res.status(500).json({
        message: `Internal Server Error`,
        err: error.message
    }))
}

// get all User 
const getAllUser = (req, res) => {

    userModel.find()
        .then((data) => {
            return res.status(200).json({
                message: "Get all User sucessfully",
                data
            })
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}

// get user by Id
const getUserById = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.userId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    userModel.findById(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Get User By Id sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message

        })
        )
}
// Update User 
const updateUser = (req, res) => {
    let id = req.params.userId
    let body = req.body

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if(!body.username){
        return res.status(400).json({
            message: `username is invalid`
        })
    }
    if(!body.firstname){
        return res.status(400).json({
            message: `firstname is invalid`
        })
    }
    if(!body.lastname){
        return res.status(400).json({
            message: `lastname is invalid`
        })
    }
    let updateUser = {
        username: body.username,
        firstname:body.firstname,
        lastname: body.lastname
    }

    userModel.findByIdAndUpdate(id, updateUser)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Update User sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}

const deleteUser = (req, res) => {
    // B1: thu thập dữ liệu
    let id = req.params.userId
    // B2:validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    // B3: thao tác CSDL
    userModel.findByIdAndDelete(id)
        .then(data => {
            if (data) {
                return res.status(200).json({
                    status: "Delete User sucessfully",
                    data
                })
            } else {
                return res.status(404).json({
                    status: "Not found any course",
                    data
                })
            }
        })
        .catch(error => res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        }))
}

module.exports = {createUser, getAllUser, getUserById, updateUser, deleteUser}