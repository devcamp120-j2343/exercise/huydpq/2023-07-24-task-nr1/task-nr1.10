const mongoose = require("mongoose");

const schema = mongoose.Schema;

const userSchema = new schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        unique: true,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    }
},{
    timestamps: true
}
)
 
module.exports = mongoose.model("user", userSchema)