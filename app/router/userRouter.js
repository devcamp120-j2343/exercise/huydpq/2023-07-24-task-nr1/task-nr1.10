const express = require("express");
const { createUser, getAllUser, getUserById, updateUser, deleteUser } = require("../controller/userController");

const userRouter = express.Router();

userRouter.post("/",createUser);

userRouter.get("/",getAllUser)

userRouter.get("/:userId",getUserById)

userRouter.put("/:userId",updateUser)

userRouter.delete("/:userId",deleteUser)


module.exports = {userRouter}