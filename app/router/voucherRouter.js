const express = require("express");
const { createVoucher, getAllVoucher, getVoucherById, updateVoucher, deleteVoucher } = require("../controller/voucherController");

const voucherRouter = express.Router();

voucherRouter.post("/",createVoucher)

voucherRouter.get("/",getAllVoucher)

voucherRouter.get("/:voucherId",getVoucherById)

voucherRouter.put("/:voucherId",updateVoucher)

voucherRouter.delete("/:voucherId",deleteVoucher)





module.exports = {voucherRouter}